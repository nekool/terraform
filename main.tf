// Configure the Google Cloud provider
// DATA LOG
provider "google" {
	credentials = "CREDENTIALS_DEVOPS.json"
	project     = "arctic-conduit-257307"
	region      = "europe-west1"
}
resource "random_id" "instance_id" {
	byte_length = 8
}
resource "google_compute_instance" "instance" {
	count		 = 3
	name         = "instance-${count.index}"
	machine_type = "n1-standard-2"
	zone         = "europe-west1-b"
	boot_disk {
		initialize_params {
			image = "debian-cloud/debian-9"
		}
	}
	metadata_startup_script = "sudo apt-get update;sudo apt install -y docker.io;sudo usermod -aG docker $USER;"
	
	metadata = {
		ssh-keys = "yann.pedron:${file("~/.ssh/id_rsa.pub")}"
	}
	 
	network_interface {
		network = "default"
		
		access_config {
			// Include this section to give the VM an external ip address
		}
	}
}
resource "google_compute_disk" "default" {
  name  = "disk"
  type  = "pd-standard"
  zone  = "europe-west1-b"
  labels = {
    environment = "dev"
  }
  physical_block_size_bytes = 4096
}
resource "google_compute_firewall" "default" {
	name    = "nginx-firewall"
	network = "default"
	 
	allow {
		protocol = "tcp"
		ports    = ["80","443"]
	}
	 
	allow {
		protocol = "icmp"
	}
}